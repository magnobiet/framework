# Framework

This project is a *clone* of [CodeIgniter](https://github.com/bcit-ci/CodeIgniter)

## Server Requirements

* PHP version 5.4

## For Development
	git submodule update --init
	composer install
	cd public/
	php -S localhost:8080

## License

[MIT License](http://magno.mit-license.org/2014) © Magno Biét
