<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| E-mail Config
|--------------------------------------------------------------------------
|
*/
$config['protocol']	    = 'smtp';
$config['smtp_host']    = 'ssl://smtp.yourdomain.com';
$config['smtp_port']    = 465;
$config['smtp_timeout'] = '5';
$config['smtp_user']    = 'no-reply@yourdomain.com';
$config['smtp_pass']    = '**********';
$config['mailtype']     = 'html';


/* End of file email.php */
/* Location: ./application/config/email.php */
