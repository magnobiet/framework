<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Google Analytics
|--------------------------------------------------------------------------
|
| Change UA-XXXXXXX-XX to be your site's ID
|
*/
$config['google_analytics'] = 'UA-XXXXXXX-XX';


/* End of file google_analytics.php */
/* Location: ./application/config/google_analytics.php */
