<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Pagination Config
|--------------------------------------------------------------------------
|
*/
$config['uri_segment'] = 3;
$config['num_links'] = 4;
$config['per_page'] = 2;
$config['use_page_numbers'] = true;
$config['page_query_string'] = false;
$config['enable_query_strings'] = false;
$config['query_string_segment'] = 'page';
$config['full_tag_open'] = '';
$config['full_tag_close'] = '';
$config['first_link'] = '&laquo;';
$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '</li>';
$config['last_link'] = '&raquo;';
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';
$config['next_link'] = 'Next';
$config['next_tag_open'] = '<li>';
$config['next_tag_close'] = '</li>';
$config['prev_link'] = 'Previous';
$config['prev_tag_open'] = '<li>';
$config['prev_tag_close'] = '</li>';
$config['cur_tag_open'] = '<li class="active"><a>';
$config['cur_tag_close'] = '</a></li>';
$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$config['display_pages'] = true;


/* End of file pagination.php */
/* Location: ./application/config/pagination.php */
