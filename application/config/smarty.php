<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Smarty Config
|--------------------------------------------------------------------------
|
|
*/
$config['compile_dir']	= APPPATH . 'cache/compiled';
$config['template_dir']	= APPPATH . 'views';
$config['config_dir']	= APPPATH . 'config';
$config['cache_dir']	= APPPATH . 'cache';
$config['caching']	    = (ENVIRONMENT === 'development') ? false : true;
$config['minify_html']  = (ENVIRONMENT === 'development') ? false : true;


/* End of file smarty.php */
/* Location: ./application/config/smarty.php */
