<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {

		parent::__construct();

	}

	public function index() {

		$data = array(
			'page_title' => 'Welcome to CodeIgniter',
			'view' => 'application/views/home.tpl',
			'controller' => 'application/controllers/Home.php',
			'user_guide' => 'http://www.codeigniter.com/user_guide/'
		);

		$this->smarty->view('home', $data);

	}

}


/* End of file Home.php */
/* Location: ./application/controllers/Home.php */
