<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('print_r2')) {

	function print_r2($data, $die = true) {

		echo '<pre style="border: 1px solid #009; padding: 10px; background: #f9f9f9;">';
		print_r($data);
		echo '</pre>';

		if ($die) {
			die();
		}

	}

}

if (!function_exists('gdc')) {

	function gdc($die = true) {

		print_r2(get_declared_classes());

		if ($die) {
			die();
		}

	}

}
