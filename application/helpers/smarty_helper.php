<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('minify_html')) {

	function minify_html($subject, Smarty_Internal_Template $template) {

		$pattern = array(
			'/\>[^\S ]+/s',                     // strip whitespaces after tags, except space
			'/[^\S ]+\</s',                     // strip whitespaces before tags, except space
			'/(\s)+/s',                         // shorten multiple whitespace sequences
			'/<!--.*?-->|\t|(?:\r?\n[ \t]*)+/s' // remove HTML comments
		);

		$replacement = array(
			'>',
			'<',
			'\\1',
			''
		);

		$subject = preg_replace($pattern, $replacement, $subject);

		return $subject;

	}

}
