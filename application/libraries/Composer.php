<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Composer {

	public function __construct() {

		$autoload_file = FCPATH . '../vendor/autoload.php';

		if (!file_exists($autoload_file)) {

			exit('Your third party folder does not have the autoload.php. Please run: <pre>php composer.phar install</pre> or <pre>composer install</pre>');

		} else {

			require_once $autoload_file;

		}

    }

}

/* End of file Composer.php */
/* Location: ./application/libraries/Composer.php */
