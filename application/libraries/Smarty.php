<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Smarty Class
 *
 * @package		CodeIgniter
 * @subpackage	Libraries
 * @category	Smarty
 * @author		Kepler Gelotte
 * @link		http://www.coolphptools.com/codeigniter-smarty
 */

class CI_Smarty extends Smarty {

	public function __construct() {

		parent::__construct();

		$ci = &get_instance();

		$ci->load->config('smarty');
		$ci->load->config('google_analytics');

		$this->compile_dir  = $ci->config->item('compile_dir');
		$this->template_dir = $ci->config->item('template_dir');
		$this->config_dir   = $ci->config->item('config_dir');
		$this->cache_dir    = $ci->config->item('cache_dir');
		$this->caching      = $ci->config->item('caching');

		$this->assign('BASE_URL', base_url());
		$this->assign('CI_VERSION', CI_VERSION);
		$this->assign('ENVIRONMENT', ENVIRONMENT);
		$this->assign('GOOGLE_ANALYTICS', $ci->config->item('google_analytics'));

		if ($ci->config->item('minify_html')) {

			$ci->load->helper('smarty');

			$this->registerFilter('output', 'minify_html');

		}

		// Assign CodeIgniter object by reference to CI
		if (method_exists($this, 'assignByRef')) {

			$ci =& get_instance();
			$this->assignByRef('ci', $ci);

		}

		log_message('debug', 'Smarty Class Initialized');

	}


	/**
	 *  Parse a template using the Smarty engine
	 *
	 * This is a convenience method that combines assign() and
	 * display() into one step.
	 *
	 * Values to assign are passed in an associative array of
	 * name => value pairs.
	 *
	 * If the output is to be returned as a string to the caller
	 * instead of being output, pass true as the third parameter.
	 *
	 * @access	public
	 * @param	string
	 * @param	array
	 * @param	bool
	 * @return	string
	 */
	public function view($template, $data = array(), $return = false) {

		foreach ($data as $key => $val) {
			$this->assign($key, $val);
		}

		if ($return == false) {

			$ci =& get_instance();

			if (method_exists($ci->output, 'set_output')) {

				$ci->output->set_output( $this->fetch($template . '.tpl') );

			} else {

				$ci->output->final_output = $this->fetch($template . '.tpl');

			}

			return;

		} else {

			return $this->fetch($template . '.tpl');

		}

	}

}

/* End of file Smarty.php */
/* Location: ./application/libraries/Smarty.php */
